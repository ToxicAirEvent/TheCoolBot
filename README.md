
#### This is a working branch for The Cool Bot.
The goal of this branch will be to allow other people to use this bot in a way that's a little more diverse.
A text fille will be read from that contains server information. Each server in the text file will be able to get queried from chat through a
global chat header with a server name parsed after it.

So the global header will be something like `serverInfo` and then it can be followed by something like `csgo10man` which would query and return the info for the server named in the text file as csgo10man.

The general idea here is that commands will be elevated to the top level with the idea of supporting more servers which have a unique identifier.

# The Cool Bot - A Discord Bot for The Cool Table.
## ...And whoever else might want to use it.

The Cool Bot was a personal project of mine developed to support querying various game or voice servers to check their status and relay information like player count and current map.

Over time I've wanted to expand the functionality of the bot to work in such a way so that anyone could download this repository and get it up and running for their own purposes.

This README.md file documents all of the recommended and required packages for The Cool Bot, or whatever you choose to name it when you get it running for yourself.

# Required and Optional NPM packages.

As mentioned above this bot is written using node.js and a number of npm packages for enhanced functionality. Please install all of the required packages and I'd recommend the optional ones as well.


## Required:
* Discord.js - https://discord.js.org/#/
  * Discord.js allows the bot to easily connect to Discords backend and function in whatever servers you invite it to.
* gamedig - https://github.com/sonicsnes/node-gamedig
  * Gamedig allows the servers to be queried and info to be returned to the chat.
* dotenv - https://github.com/motdotla/dotenv
	- Allows node.js to have a .env file which contains varialbes and user configured settings which will be used throughout the application.

## Optional:
* nodemon - https://nodemon.io/
  * nodemon is a dameon service for nodejs. It allows node files to be run from the command line with variable arguements added. It also has crash recovery built in. Discord.js does not recover well if it loses connection to discord server.
Running the bot via nodemon will restart/recover the bot in the event of a crash or connection loss. It isn't necessary to the bots functionality, but is recommended.
Additionally nodemon will automatically restart the bot should any files in the directory/repository change.

# Getting things up and running
1. Have [node.js](https://nodejs.org/en/ "node.js") installed on your computer or server that you wish to run the bot on.
1. Download or clone in this bots repository. Master branch is recommended.
1. Use NPM to install all of the required node modules, install any optional ones you like as well.
1. Configure your .env file. You can use .env.example as a starting place.
	- There are some additional instructions in .env.example which highlight how to get an API token to allow the bot to connect to discord.
1. Open the servers.txt and enter the servers you want to be available for query in by your bot.
	- Format for servers.txt is as follows. One server exist inbetween each <> set. Each paramater is required.
	- The paramaters are `<trigger|Game Type|Server IP|Server Port|Human Readable Name of the server>`
	- **Note:** Game Type is the value inbetween the () next to each game listed in the [supported games list for the Gamedig library.](https://github.com/sonicsnes/node-gamedig#games-list "supported games list for the Gamedig library.") If your game does not exist on this list the bot cannot interface with it. Also be aware some game require special server settings to correctly interface with the bot.
1. Once your .env and servers.txt files are configured simply run `node MultiServer.js` or `nodemon MultiServer.js` and your bot will be up and running! Give it a test in your Discord server after you've invited it!

# Using the bot
Using the bot once you've got it online is simple.
Start a message with the `GLOBAL_LISTENER` you defined in your .env file. Then type the name of a trigger you entered in the servers.txt file.

**Example:** `gameinfo csgoRetakes` in chat will query and return infor for the server deinfed under the trigger csgoRetakes in your servers.txt file.

## Future Updates:
- Better support for game servers which don't return full data sets. A good exmple of this is a game like minecraft. Minecraft only returns a player count and does not return the world name as a map name. This casues the map name to be undefined when the promise is resolved. The bot works fine but this looks goofy.
- Better error handling overall. I don't do a great job currently at catching bad returns or handling things like server offline messages when one can't be reached.
- Handle user errors better. Right now if someone enters a server that doesn't exist not much happens. Ideally a help function will get added at some point which will return "Did you mean?" style messages when the user enters something close to a real command or server but not a valid one.

#### License
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.

For commercial use please contact me at jackemceachern[at]gmail.com

#### Support
This bot is provided as is with no warranties or support of any kind. Feel free to open any issues, or bring any bugs or feature request to my attention. I will do my best to help where I can but it should be assumed no support will be provided.
