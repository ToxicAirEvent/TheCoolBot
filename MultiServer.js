//Required modules in order for the bot to function. Check the readme.md for what they are and what they do.
const Discord = require('discord.js');
require('dotenv').config();
const Gamedig = require('gamedig');

//Load in custom gamedig queries.
var serverInfo = require("./serverInfo");

// create an instance of a Discord Client, and call it bot
const bot = new Discord.Client();

// the token of your bot - https://discordapp.com/developers/applications/me
const token = process.env.DISCORD_API_TOKEN;

// log our bot in
bot.login(token);

// the ready event is vital, it means that your bot will only start reacting to information
// from Discord _after_ ready is emitted.
bot.on('ready', () => {
  console.log('Up and running.');
});

var startsWithString = process.env.GLOBAL_LISTENER;
startsWithString = startsWithString.toLowerCase()

console.log("Messages starting with: " + startsWithString + " will trigger the bot.");

// create an event listener for messages
bot.on('message', message => {
	
	console.log("------------------------------");

	//See if information about the CSGO server is being requested.
	if(message.content.toLowerCase().startsWith(startsWithString) == true && message.content.length <= 25){
		console.log("String starts with " + startsWithString + ".");
		console.log("It is also less than 25 characters.");
		
		var checkForServerChatTrigger = message.content.toLowerCase().split(' ');
		var chat_trigger = checkForServerChatTrigger[1];
		chat_trigger = chat_trigger.toLowerCase().trim();
		console.log("Looking for a server with the chat trigger: " + chat_trigger);

		if(servers.hasOwnProperty(chat_trigger)){
			console.log("The chat trigger " + chat_trigger + " was found.");
			message.channel.send("Information for the: " + servers[chat_trigger].serverHumanName);

			//Query and resolve a promise for the server the user in requesting info on.
			console.log("Server properties: " + servers[chat_trigger].gameType + " IP:" + servers[chat_trigger].serverIP + " Port:" + servers[chat_trigger].serverPort);
			var resolveServerInfo = Promise.resolve(serverInfo.serverQueryInfo(servers[chat_trigger].gameType,servers[chat_trigger].serverIP,servers[chat_trigger].serverPort));
			//If the promise resolves send information back to the chat.
			resolveServerInfo.then(function(data){
				message.channel.send("There are " + data.playerCount + " players currently on the server. \n The current map is: " + data.currentMap);
				message.channel.send("If you wish to connect to the server the IP is: " + servers[chat_trigger].serverIP + " and the port is: " + servers[chat_trigger].serverPort + "\nServer connection info as one easy to copy address: " + servers[chat_trigger].serverIP + ":" + servers[chat_trigger].serverPort);
			});
		}else{
			console.log("The chat trigger could not be found.");
		}		
	}else{
		console.log("Message invalid. Doing nothing.");
		console.log("The message was: " + message.content);
		console.log("It was " + message.content.length + " characters in length.");
	}

	console.log("------------------------------");
	
});


//Create an object which will contain the server information imported from servers.txt
var servers = {};

// Read the file and print its contents.
var fs = require('fs'), filename = 'servers.txt';
fs.readFile(filename, 'utf8', function(err, data) {
  if (err) throw err;
  console.log('VALID: ' + filename);

	/*
	* Quick and easy way to split out each server listed. Thanks Mr. Stackoverflow.
	* https://stackoverflow.com/questions/25967141/get-multiple-strings-between-two-matched-characters-using-jquery-regular-express
	*/
  var text_file_splits = [];
	data.replace(/<(.*?)>/g, function(g0,g1){
	  text_file_splits.push(g1);
	});

  for(i = 0;i < text_file_splits.length;i++){
  	var this_server = text_file_splits[i].split("|");
  	var server_chat_trigger = this_server[0];
  	var single_server = {gameType:this_server[1],serverIP:this_server[2],serverPort:this_server[3],serverHumanName:this_server[4]};
  	servers[server_chat_trigger] = single_server;
  }

  console.log("----------REGISTERED SERVERS----------");
  console.log(servers);
  console.log("----------END REGISTERED SERVERS----------");
});