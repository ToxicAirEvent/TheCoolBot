//Basic message the outputs the bot help message informing users of commands they can use with the bot.
var botHelp = "<:GitOut:282565595839463425>**BOT HELP:** \n It looks like I didn't find what your asking for.\n Try:\n csgo 10man - For 10 Man server info.\n csgo 1v1 - For 1v1 Arena server info.\n minecraft server - For minecraft server info.\n minecraft commands - For minecraft server commands. \n Full information at: http://cooltable.org/";

//Sets a header message to be used for all minecraft server messages.
var minecraftHeader = "<:Legosimulator:295646503471415298>**MINECRAFT SERVER INFO:**";

//Base server return information that is returned with all (or at least most minecraft server request.
var minecraftServerInfo = "* Server Name: The Cool Table \n * Server address: minecraft.cooltable.org \n * Full Rules and Information: http://cooltable.org";

var csgoHeader = "<:dinked:269569184344702978>The Cool Table - CSGO Server Information - <:ItsPinnedDamnIt:278009192571338752>";

var pugServerInfo = "Open your console with the `~` key and paste the following line completely to connect to the 10 man server: _connect 138.128.20.213:27045; password TheCoolTable2017_ \n To connect faster and be up to date subscribe to the server workshop collection: http://steamcommunity.com/sharedfiles/filedetails/?id=587753305";

var csgo1v1Info = "Open your console with the `~` key and paste the following line completely to connect to the 1v1 server: _connect 138.128.20.213:27025_ \n To connect faster and be up to date subscribe to the server workshop collection: http://steamcommunity.com/sharedfiles/filedetails/?id=605020025";

//Export modules so they can be accessed in other files.
module.exports.minecraftHeader = minecraftHeader;
module.exports.minecraftServerInfo = minecraftServerInfo;
module.exports.csgoHeader = csgoHeader;
module.exports.pugServerInfo = pugServerInfo;
module.exports.csgo1v1Info = csgo1v1Info;
module.exports.botHelp = botHelp;