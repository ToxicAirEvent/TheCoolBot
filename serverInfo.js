//Import Gamedig package https://github.com/sonicsnes/node-gamedig
const Gamedig = require('gamedig');

//Query Minecraft Server
var serverInfo = function(gameType,serverIP,serverPort){
	
	//Query server based on chatTrigger input.
	  return Gamedig.query({
			type: gameType,
			host: serverIP,
			port: serverPort
		})
		.then(function(state){
			var playersOnline = state.players.length;
			var currentMap = state.map;

			//Vomit a bunch of error checking.
			console.log("Players Online: " + playersOnline);
			console.log("Server map: " + currentMap);

			return {playerCount:playersOnline,currentMap:currentMap};
		})
		.catch(function(error){
			console.error(error);
			return 'Server connection error';
		});
	//End Query	
}

//Export the serverInfo function for use in TheCoolBot.js
module.exports.serverQueryInfo = serverInfo;